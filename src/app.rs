use glib::object::IsA;
use glib::translate::*;

use App;

pub trait AppExtManual: 'static {
    pub fn search_matches_all(&self, search: [&str]) -> i32;
}


impl<O: IsA<App>> AppExtManual for O {
    pub fn search_matches_all(&self) -> i32 {
        // blablabla
        0
    }
}
