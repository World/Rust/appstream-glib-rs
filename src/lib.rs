#[macro_use]
extern crate glib;
#[macro_use]
extern crate bitflags;

extern crate fragile;
extern crate gio;
extern crate gio_sys;
extern crate glib_sys;
extern crate appstream_glib_sys as app_stream_glib_sys;
extern crate lazy_static;
extern crate gobject_sys;
extern crate gdk_pixbuf_sys;
extern crate gdk_pixbuf;
extern crate libc;

pub use glib::Error;

pub use crate::auto::*;
mod auto;
mod store;
pub mod prelude;
pub use prelude::*;