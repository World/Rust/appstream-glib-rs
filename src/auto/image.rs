// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// DO NOT EDIT

#[cfg(any(feature = "v0_1_6", feature = "dox"))]
use Error;
#[cfg(any(feature = "v0_2_2", feature = "dox"))]
use ImageAlphaFlags;
use ImageKind;
#[cfg(any(feature = "v0_5_6", feature = "dox"))]
use ImageLoadFlags;
#[cfg(any(feature = "v0_1_6", feature = "dox"))]
use ImageSaveFlags;
use app_stream_glib_sys;
#[cfg(any(feature = "v0_1_6", feature = "dox"))]
use gdk_pixbuf;
use glib::GString;
use glib::object::IsA;
use glib::translate::*;
use std::fmt;
#[cfg(any(feature = "v0_1_6", feature = "dox"))]
use std::ptr;

glib_wrapper! {
    pub struct Image(Object<app_stream_glib_sys::AsImage, app_stream_glib_sys::AsImageClass, ImageClass>);

    match fn {
        get_type => || app_stream_glib_sys::as_image_get_type(),
    }
}

impl Image {
    pub fn new() -> Image {
        unsafe {
            from_glib_full(app_stream_glib_sys::as_image_new())
        }
    }

    pub fn kind_from_string(kind: &str) -> ImageKind {
        unsafe {
            from_glib(app_stream_glib_sys::as_image_kind_from_string(kind.to_glib_none().0))
        }
    }

    pub fn kind_to_string(kind: ImageKind) -> Option<GString> {
        unsafe {
            from_glib_none(app_stream_glib_sys::as_image_kind_to_string(kind.to_glib()))
        }
    }
}

impl Default for Image {
    fn default() -> Self {
        Self::new()
    }
}

pub const NONE_IMAGE: Option<&Image> = None;

pub trait ImageExt: 'static {
    #[cfg(any(feature = "v0_5_7", feature = "dox"))]
    fn equal<P: IsA<Image>>(&self, image2: &P) -> bool;

    #[cfg(any(feature = "v0_2_2", feature = "dox"))]
    fn get_alpha_flags(&self) -> ImageAlphaFlags;

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn get_basename(&self) -> Option<GString>;

    fn get_height(&self) -> u32;

    fn get_kind(&self) -> ImageKind;

    #[cfg(any(feature = "v0_5_14", feature = "dox"))]
    fn get_locale(&self) -> Option<GString>;

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn get_md5(&self) -> Option<GString>;

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn get_pixbuf(&self) -> Option<gdk_pixbuf::Pixbuf>;

    fn get_url(&self) -> Option<GString>;

    fn get_width(&self) -> u32;

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn load_filename(&self, filename: &str) -> Result<(), Error>;

    #[cfg(any(feature = "v0_5_6", feature = "dox"))]
    fn load_filename_full(&self, filename: &str, dest_size: u32, src_size_min: u32, flags: ImageLoadFlags) -> Result<(), Error>;

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn save_filename(&self, filename: &str, width: u32, height: u32, flags: ImageSaveFlags) -> Result<(), Error>;

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn save_pixbuf(&self, width: u32, height: u32, flags: ImageSaveFlags) -> Option<gdk_pixbuf::Pixbuf>;

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn set_basename(&self, basename: &str);

    fn set_height(&self, height: u32);

    fn set_kind(&self, kind: ImageKind);

    #[cfg(any(feature = "v0_5_14", feature = "dox"))]
    fn set_locale(&self, locale: &str);

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn set_pixbuf(&self, pixbuf: &gdk_pixbuf::Pixbuf);

    fn set_url(&self, url: &str);

    fn set_width(&self, width: u32);
}

impl<O: IsA<Image>> ImageExt for O {
    #[cfg(any(feature = "v0_5_7", feature = "dox"))]
    fn equal<P: IsA<Image>>(&self, image2: &P) -> bool {
        unsafe {
            from_glib(app_stream_glib_sys::as_image_equal(self.as_ref().to_glib_none().0, image2.as_ref().to_glib_none().0))
        }
    }

    #[cfg(any(feature = "v0_2_2", feature = "dox"))]
    fn get_alpha_flags(&self) -> ImageAlphaFlags {
        unsafe {
            app_stream_glib_sys::as_image_get_alpha_flags(self.as_ref().to_glib_none().0)
        }
    }

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn get_basename(&self) -> Option<GString> {
        unsafe {
            from_glib_none(app_stream_glib_sys::as_image_get_basename(self.as_ref().to_glib_none().0))
        }
    }

    fn get_height(&self) -> u32 {
        unsafe {
            app_stream_glib_sys::as_image_get_height(self.as_ref().to_glib_none().0)
        }
    }

    fn get_kind(&self) -> ImageKind {
        unsafe {
            from_glib(app_stream_glib_sys::as_image_get_kind(self.as_ref().to_glib_none().0))
        }
    }

    #[cfg(any(feature = "v0_5_14", feature = "dox"))]
    fn get_locale(&self) -> Option<GString> {
        unsafe {
            from_glib_none(app_stream_glib_sys::as_image_get_locale(self.as_ref().to_glib_none().0))
        }
    }

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn get_md5(&self) -> Option<GString> {
        unsafe {
            from_glib_none(app_stream_glib_sys::as_image_get_md5(self.as_ref().to_glib_none().0))
        }
    }

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn get_pixbuf(&self) -> Option<gdk_pixbuf::Pixbuf> {
        unsafe {
            from_glib_none(app_stream_glib_sys::as_image_get_pixbuf(self.as_ref().to_glib_none().0))
        }
    }

    fn get_url(&self) -> Option<GString> {
        unsafe {
            from_glib_none(app_stream_glib_sys::as_image_get_url(self.as_ref().to_glib_none().0))
        }
    }

    fn get_width(&self) -> u32 {
        unsafe {
            app_stream_glib_sys::as_image_get_width(self.as_ref().to_glib_none().0)
        }
    }

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn load_filename(&self, filename: &str) -> Result<(), Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let _ = app_stream_glib_sys::as_image_load_filename(self.as_ref().to_glib_none().0, filename.to_glib_none().0, &mut error);
            if error.is_null() { Ok(()) } else { Err(from_glib_full(error)) }
        }
    }

    #[cfg(any(feature = "v0_5_6", feature = "dox"))]
    fn load_filename_full(&self, filename: &str, dest_size: u32, src_size_min: u32, flags: ImageLoadFlags) -> Result<(), Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let _ = app_stream_glib_sys::as_image_load_filename_full(self.as_ref().to_glib_none().0, filename.to_glib_none().0, dest_size, src_size_min, flags.to_glib(), &mut error);
            if error.is_null() { Ok(()) } else { Err(from_glib_full(error)) }
        }
    }

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn save_filename(&self, filename: &str, width: u32, height: u32, flags: ImageSaveFlags) -> Result<(), Error> {
        unsafe {
            let mut error = ptr::null_mut();
            let _ = app_stream_glib_sys::as_image_save_filename(self.as_ref().to_glib_none().0, filename.to_glib_none().0, width, height, flags.to_glib(), &mut error);
            if error.is_null() { Ok(()) } else { Err(from_glib_full(error)) }
        }
    }

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn save_pixbuf(&self, width: u32, height: u32, flags: ImageSaveFlags) -> Option<gdk_pixbuf::Pixbuf> {
        unsafe {
            from_glib_full(app_stream_glib_sys::as_image_save_pixbuf(self.as_ref().to_glib_none().0, width, height, flags.to_glib()))
        }
    }

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn set_basename(&self, basename: &str) {
        unsafe {
            app_stream_glib_sys::as_image_set_basename(self.as_ref().to_glib_none().0, basename.to_glib_none().0);
        }
    }

    fn set_height(&self, height: u32) {
        unsafe {
            app_stream_glib_sys::as_image_set_height(self.as_ref().to_glib_none().0, height);
        }
    }

    fn set_kind(&self, kind: ImageKind) {
        unsafe {
            app_stream_glib_sys::as_image_set_kind(self.as_ref().to_glib_none().0, kind.to_glib());
        }
    }

    #[cfg(any(feature = "v0_5_14", feature = "dox"))]
    fn set_locale(&self, locale: &str) {
        unsafe {
            app_stream_glib_sys::as_image_set_locale(self.as_ref().to_glib_none().0, locale.to_glib_none().0);
        }
    }

    #[cfg(any(feature = "v0_1_6", feature = "dox"))]
    fn set_pixbuf(&self, pixbuf: &gdk_pixbuf::Pixbuf) {
        unsafe {
            app_stream_glib_sys::as_image_set_pixbuf(self.as_ref().to_glib_none().0, pixbuf.to_glib_none().0);
        }
    }

    fn set_url(&self, url: &str) {
        unsafe {
            app_stream_glib_sys::as_image_set_url(self.as_ref().to_glib_none().0, url.to_glib_none().0);
        }
    }

    fn set_width(&self, width: u32) {
        unsafe {
            app_stream_glib_sys::as_image_set_width(self.as_ref().to_glib_none().0, width);
        }
    }
}

impl fmt::Display for Image {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Image")
    }
}
